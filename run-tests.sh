#!/bin/sh

set -e

cargo build --bin cargo-publish-all
cargo test -- --color always --nocapture
